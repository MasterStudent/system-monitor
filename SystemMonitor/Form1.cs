﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Management;
using System.Management.Instrumentation;
using System.Collections.Specialized;
using System.Threading;

namespace SystemMonitor
{
    public partial class Form1 : Form
    {
        NotifyIcon hddNotify;
        Icon activeIcon;
        Icon idleIcon;
        Thread hddLedWorker;

        public Form1()
        {
            InitializeComponent();

            //Load Icons
            activeIcon = new Icon("hddActive.ico");
            idleIcon = new Icon("hddIdle.ico");

            //Create notification icon, assign its initial value and display it
            hddNotify = new NotifyIcon();
            hddNotify.Icon = idleIcon;
            hddNotify.Visible = true;

            //Create context menu items 
            MenuItem progName = new MenuItem("System Monitor V0.1");
            MenuItem exit = new MenuItem("Exit");

            //Create context menu and add the items
            ContextMenu contextMenu = new ContextMenu();
            contextMenu.MenuItems.Add(progName);
            contextMenu.MenuItems.Add(exit);

            //Add context menu to the notification tray icon
            hddNotify.ContextMenu = contextMenu;

            //Link instructions to buttons
            exit.Click += exit_Click;

            //Hide the form
            this.WindowState = FormWindowState.Minimized;
            this.ShowInTaskbar = false;

            //Start Threads
            hddLedWorker = new Thread(new ThreadStart(hddActivity));
            hddLedWorker.Start();
        }

        //Process for closing the programe
        private void exit_Click(object sender, EventArgs e)
        {
            //Close the app
            hddLedWorker.Abort();
            hddNotify.Dispose();
            this.Close();
        }

        //This thread deals with the hdd activity notification
        public void hddActivity()
        {
            ManagementClass driveData = new ManagementClass("Win32_PerfFormattedData_PerfDisk_PhysicalDisk");
            ManagementObjectCollection driveDataCollection;

            try
            {
                while (true)
                {
                    //Connect to the drive performance instance
                    driveDataCollection = driveData.GetInstances();

                    foreach (ManagementObject obj in driveDataCollection)
                    {
                        //Filter out individual instances
                        if (obj["name"].ToString() == "_Total")
                        {
                            if (Convert.ToUInt64(obj["DiskBytesPersec"]) > 0)
                            {
                                //Switch notification icon to active
                                hddNotify.Icon = activeIcon;
                            }
                            else
                            {
                                //Switch notification icon to idle
                                hddNotify.Icon = idleIcon;
                            }
                        }
                    }

                    Thread.Sleep(10);
                }
            }
            catch (ThreadAbortException tbe)
            {
                driveData.Dispose();
            }
            
        }
    }
}
